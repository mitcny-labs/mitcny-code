<img src="https://assets.gitlab-static.net/jaydamask/mitcny-iconography/raw/master/artwork/assets/mitcny-labs-tile-logo.1500x.png" alt="drawing" height="150"/>


# MITCNY::code

Codebase and static data for the analyzing and processing of club data.


## License 

[![licence shield](https://img.shields.io/badge/license-MIT%20Club%20of%20New%20York-blue)](http://mitclub.org)

This is a **private** repo. The owner is the [MIT Club of New York](http://mitclub.org). All rights reserved. 


