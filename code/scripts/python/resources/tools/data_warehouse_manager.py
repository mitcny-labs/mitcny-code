"""
-------------------------------------------------------------------------------

Data-warehouse manager

-------------------------------------------------------------------------------
"""

import logging
import os
import posixpath
import re


class DataWarehouseManager(object):
    """
    The data warehouse for the mitcny-code repo is the mitcny-data repo.
    Because the latter is factored out of the -code repo, for data-security
    concerns, the link between -code and -data is a symlink having basename
    `data-warehouse`.

    This manager validates that the symlink exists where it is expected,
    and is the provider of the relpath to the warehouse.

    Note that PYTHONPATH must include $ROOT/mitcny-code/code/scripts/python.
    """

    def __init__(self):
        """
        Throws if the $ROOT/mitcny-code/code/scripts/python part segment
        cannot be found.

        If successful, this class stores locally

            repo_path: abspath to this repo's root dir
            py_module_path: abspath to this repo's python dir
            data_warehouse_path: abspath to the data-warehouse symlink

        """

        # discover the encompassing path or die
        pypaths = os.environ["PYTHONPATH"].split(":")
        discovered = False
        for py_module_path in pypaths:

            if (
                re.search("mitcny-code/code/scripts/python", py_module_path)
                is not None
            ):
                discovered = True
                break  # captures py_module_path

        if not discovered:

            msg = "{0} {1}".format(
                "The PYTHONPATH envvar must be set in the form",
                "$ROOT/mitcny-code/code/sripts/python",
            )
            logging.error(msg)
            raise RuntimeError(msg)

        # persist the discovered path
        self._py_module_path = py_module_path

        # back out repo path
        repo_path = posixpath.abspath(
            posixpath.join(py_module_path, "../../..")
        )
        self._repo_path = repo_path

        # now validate that the data-warehouse path is there
        dwh_path = posixpath.join(py_module_path, "resources/data-warehouse")

        if not posixpath.exists(dwh_path):

            msg = "The resources/data-warehouse/ path is not available."
            logging.error(msg)
            raise RuntimeError(msg)

        # persist the data-warehouse path
        self._data_warehouse_path = dwh_path

        # log
        logging.info("DataWarehouseManager constructed")
        logging.info(
            "DataWarehouseManager: discovered repo path: {0}".format(repo_path)
        )
        logging.info(
            "DataWarehouseManager: discovered python-module path: {0}".format(
                py_module_path
            )
        )
        logging.info(
            "DataWarehouseManager: discovered dwh path: {0}".format(dwh_path)
        )

    @staticmethod
    def validate_file_or_die(full_filename: str):
        """Throws if full_filename is not discovered"""

        if not posixpath.exists(full_filename):

            msg = "File '{0}' is not found".format(full_filename)
            logging.error(msg)
            raise RuntimeError(msg)

    def get_mailing_list_data_path(self) -> str:
        """mitcny-data path to the mitaa mailing-list data"""

        return posixpath.join(
            self._data_warehouse_path, "mitaa-data/mailing-list-data"
        )

    def get_mailing_artifact_path(self) -> str:
        """path to mitcny mailing-list artifact path"""

        return posixpath.join(
            self._data_warehouse_path, "mitcny-artifacts/mailing-list-results"
        )

    def get_repo_path(self) -> str:
        """returns the repo's path"""

        return self._repo_path

    def get_python_module_path(self) -> str:
        """returns the python module path"""

        return self._py_module_path

    def get_data_warehouse_path(self) -> str:
        """returns the data-warehouse path"""

        return self._data_warehouse_path
