"""
-------------------------------------------------------------------------------

Builder for the print mailing lists.

-------------------------------------------------------------------------------
"""

import logging
import numpy as np
import pandas as pd
import posixpath

from resources.tools.data_warehouse_manager import DataWarehouseManager


# metadata for the print-mailing builder
metadata_structures = {
    "zipcodes_dtypes": {
        "zipcode": "string",
        "zipcode_name": "string",
        "county": "string",
        "state": "string",
    },
    "mitaa_src_data_dtypes": {
        "advance_id": "int",
        "pref_mail_name": "string",
        "pref_class_year": "int",
        "company_name": "string",
        "street1": "string",
        "street2": "string",
        "street3": "string",
        "city": "string",
        "state_code": "string",
        "zipcode_full": "string",
        "foreign_cityzip": "string",
        "country": "string",
    },
    "task_map": {
        "build-alumni-mailing-list": "alumni",
        "build-parent-mailing-list": "parents",
    },
}


class PrintMailingBuilder(object):
    """
    Builds a print-mailing dataset and renders it for the printers and for
    the archive.
    """

    def __init__(
        self,
        task: str,
        year: str,
        src_fn: str,
        dwh_manager: DataWarehouseManager,
    ):

        # validate task
        if task not in metadata_structures["task_map"].keys():
            msg = "Unknown task: {0}.  Provide a valid field to the --task option.".format(
                task
            )
            logging.error(msg)
            raise RuntimeError(msg)

        # capture as locals
        self._task = task
        self._year = year
        self._src_fn = src_fn
        self._dwh_manager = dwh_manager

        # additional local
        self._task_group = metadata_structures["task_map"][task]

        # log
        logging.info("PrintMailingBuilder constructed")

    def build_mailing_list(self):
        """
        Build, validates, and saves mailing-list related data.
        Inputs:
            1) alumni or parents data file from the mitaa
            2) zipcode file (in this repo)

        Outputs:
            1) raw file of zipcode-based valid mitaa-src records
            2) raw file of zipcode-based invalid mitaa-src records
            3) printer-ready file of valid mitaa-src (=mitcny) records
        """

        # fetch input dataframes
        logging.info("fetching src dataframes...")
        df_z = self.fetch_zipcode_dataframe()
        df_a = self.fetch_mitaa_src_dataframe()

        # merge on zipcode
        logging.info("merging dataframes...")
        d = self.merge_mitaa_src_and_zipcode_dfs(df_a, df_z)

        # save the raw data that was split into valid/invalid sets
        logging.info("saving raw valid/invalid dataframes...")
        self.save_raw_mitaa_validated_dataframes(d)

        # save printer-ready file
        logging.info("creating and saving printer-ready mail list...")
        self.render_and_save_mitcny_mailing_list(d["valid"])

    def fetch_zipcode_dataframe(self) -> pd.DataFrame:
        """
        The csv file has the column names

            zipcode -- index
            name
            county
            state
            [i] (added here from the csv)

        Sample records are

            06807,,Fairfield County,Connecticut,0
            06820,,Fairfield County,Connecticut,1
            06830,,Fairfield County,Connecticut,2
        """

        # build filename
        full_filename = posixpath.join(
            self._dwh_manager.get_repo_path(),
            "reference-data/zip-codes-and-maps/mitcny-regional-zipcodes.csv",
        )

        # load into pandas dataframe
        df_zipcodes = pd.read_csv(
            full_filename,
            header="infer",
            index_col="zipcode",
            dtype=metadata_structures["zipcodes_dtypes"],
        )

        # append i column
        df_zipcodes["i"] = range(df_zipcodes.shape[0])

        # log
        logging.info(
            "loaded zipcode dataframe with {0} records".format(
                df_zipcodes.shape[0]
            )
        )

        # return
        return df_zipcodes

    def fetch_mitaa_src_dataframe(self) -> pd.DataFrame:
        """
        The dataframe has the column names

            advance_id
            pref_mail_name
            pref_class_year
            company_name
            street1
            street2
            street3
            city
            state_code
            zipcode_full
            foreign_cityzip
            country
            [zipcode]  (added here from the csv)

        Sample records are
            19...00870,Dr Zakhar x xxxxxx,19.1, ,NN River Rd, , ,Cos Cob,CT,06807, ,
            19...01250,Mr Daniel y yyyyyy,19.5, ,MM Ridge Rd, , ,Cos Cob,CT,06807, ,
        """

        # build filename
        full_filename = posixpath.join(
            self._dwh_manager.get_mailing_list_data_path(), self._src_fn
        )

        # load into pandas dataframe
        df_alumni = pd.read_csv(
            full_filename,
            header="infer",
            dtype=metadata_structures["mitaa_src_data_dtypes"],
        )

        # strip zipcode-full down to zipcode in new column
        df_zipcode = (
            df_alumni["zipcode_full"]
            .map(lambda x: x[:5])  # take 1st 5 chars
            .reset_index()
            .rename({"zipcode_full": "zipcode"}, axis="columns")
            .drop(columns='index')
        )

        # lj on index
        df_alumni = df_alumni.join(df_zipcode)

        # log
        logging.info("discovered mitaa-src file {0}".format(full_filename))
        logging.info(
            "loaded mitaa-src dataframe with {0} records".format(
                df_alumni.shape[0]
            )
        )

        # return
        return df_alumni

    def merge_mitaa_src_and_zipcode_dfs(
        self, df_alumni: pd.DataFrame, df_zipcodes: pd.DataFrame
    ) -> dict:
        """
        Merges mitaa-src data with zipcode to create two sets:
            1) alumni/parent records with valid regional zipcodes
            2) alumni/parent records without valid regional zipcodes
        """

        # remove the respective indices and left-merge zipcodes onto alumni
        df_merge = (
            df_alumni
            .merge(df_zipcodes.reset_index(), how="left", on="zipcode")
        )

        # separate into matched and unmatched based on non-nan values of col 'i'
        # remove 'i' after merge since it's purpose is now served
        df_alumni_regional = df_merge[~np.isnan(df_merge["i"])].drop(
            "i", axis="columns"
        )
        df_alumni_not_regional = df_merge[np.isnan(df_merge["i"])].drop(
            "i", axis="columns"
        )

        # pack
        d = {"valid": df_alumni_regional, "invalid": df_alumni_not_regional}

        # log
        logging.info(
            "record count (valid, invalid): ({0},{1})".format(
                d["valid"].shape[0], d["invalid"].shape[0]
            )
        )

        # return
        return d

    def save_raw_mitaa_validated_dataframes(self, dfs_validated: dict):
        """
        For dfs_validated having 'volid' and 'invalid' dataframes,
        saves these to csv files for archiving.
        """

        # fetch archive path
        arch_path = self._dwh_manager.get_mailing_artifact_path()
        task_group = self._task_group

        # build filenames
        fns = {
            "valid": "nyc_valid_regional.raw.{0}.{1}.csv".format(
                task_group, self._year
            ),
            "invalid": "nyc_invalid_regional.raw.{0}.{1}.csv".format(
                task_group, self._year
            ),
        }

        # iter over keys, save each table
        for k in dfs_validated.keys():

            # build full filename
            ffn = posixpath.join(arch_path, fns[k])

            # log
            logging.info("saving raw file: {0}".format(ffn))

            # save
            dfs_validated[k].to_csv(ffn, index=False)

    def render_and_save_mitcny_mailing_list(self, df_validated: pd.DataFrame):
        """
        This is simple b/c all we need to do is call out columns

            pref_mail_name
            street1
            street2
            street3
            city
            state_code
            zipcode_full

        from the valid table and save that.
        """

        # fetch archive path
        arch_path = self._dwh_manager.get_mailing_artifact_path()

        # build full filename
        fn = "printers_full_mailing_file.{0}.{1}.csv".format(
            self._task_group, self._year
        )
        ffn = posixpath.join(arch_path, fn)

        # cols to save
        cols = [
            "pref_mail_name",
            "street1",
            "street2",
            "street3",
            "city",
            "state_code",
            "zipcode_full",
        ]

        # log
        logging.info("saving printer's file: {0}".format(ffn))

        # save
        df_validated.reset_index().to_csv(ffn, columns=cols, index=False)
