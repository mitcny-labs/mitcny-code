"""
-------------------------------------------------------------------------------

Runner to prepare, render, and analyze annual alumni data for the New York
region.

This job is to be run on the shell cmdline via

$ python print_mailing_runner.py --task TASK --year YEAR --src-fn SRC-FN

For example (without the line breaks),

$ python print_mailing_runner.py
    --task build-alumni-mailing-list
    --year 2019
    --src-fn nyc_zipcode_based_mailing_list.20190730.csv

Note that PYTHONPATH must include $ROOT/mitcny-code/code/scripts/python.

-------------------------------------------------------------------------------
"""


import argparse
import logging
import os
import platform
import posixpath
import subprocess
import sys

from resources.tools.data_warehouse_manager import DataWarehouseManager
from resources.tools.print_mailing_builder import PrintMailingBuilder


# -----------------------------------------------------------------------------
def dispatch(pm_builder: PrintMailingBuilder):
    """Executes a job based on the context"""

    logging.info("executing job...")

    pm_builder.build_mailing_list()

    logging.info("...job completed.")


# -----------------------------------------------------------------------------
def validate_source_files_or_die(cmdline_args) -> DataWarehouseManager:
    """Validates that the source file(s) exist, or throws"""

    # can throw
    dwh_manager = DataWarehouseManager()

    # build src full filename
    src_ffn = posixpath.join(
        dwh_manager.get_mailing_list_data_path(), cmdline_args.src_fn
    )

    DataWarehouseManager.validate_file_or_die(src_ffn)

    return dwh_manager


# -----------------------------------------------------------------------------
def log_cmdline_args(cmdline_args):
    """Commit the cmdline args to the logfile"""

    logging.info("  task   : " + str(cmdline_args.task))
    logging.info("  year   : " + str(cmdline_args.year))
    logging.info("  src-fn : " + str(cmdline_args.src_fn))
    logging.info("  dryrun : " + str(cmdline_args.dryrun))


# -----------------------------------------------------------------------------
def display_usage_and_die(parser=None):
    """Standard out of command-line usage"""

    if parser:
        parser.print_help()
        print(
            "Also, you must have $ROOT/mitcny-code/code/scripts/python in your PYTHONPATH."
        )

    sys.exit(0)


# -----------------------------------------------------------------------------
def get_command_line_args():
    """Configure command-line arguments"""

    # make parser instance
    parser = argparse.ArgumentParser(prog="runner")

    # hack from https://stackoverflow.com/questions/24180527/
    #               argparse-required-arguments-listed-under-optional-arguments
    optional = parser._action_groups.pop()
    required = parser.add_argument_group("required arguments")

    # make options list
    # required
    required.add_argument(
        "--task",
        dest="task",
        required=True,
        help="{build-alumni-mailing-list|build-parent-mailing-list}",
    )
    required.add_argument(
        "--year", dest="year", required=True, help="the year to process"
    )
    required.add_argument(
        "--src-fn",
        dest="src_fn",
        required=True,
        help="filename of mitaa alumni to process",
    )
    # optional
    optional.add_argument(
        "--dryrun",
        dest="dryrun",
        help="[optional] perform dryrun",
        action="store_true",
    )

    # ref above-mentioned hack
    parser._action_groups.append(optional)

    # construct the args from the cmdline parser
    args = parser.parse_args()

    # test input status
    if not all([args.task, args.year, args.src_fn]):
        display_usage_and_die(parser)

    # return and continue
    return args


# -----------------------------------------------------------------------------
def main(options=None):
    """Entry point to application"""

    # get cmdline opts
    cmdline_args = get_command_line_args()

    # log starter
    logging.info("print_mailing_runner.py executed by:")
    logging.info("  host: " + str(os.uname()))
    logging.info("  py  : " + str(platform.python_version()))
    logging.info("  by  : " + str(subprocess.getoutput("whoami")))
    logging.info("")
    log_cmdline_args(cmdline_args)

    # validation
    dwh_manager = validate_source_files_or_die(cmdline_args)

    # make a builder
    pm_builder = PrintMailingBuilder(
        cmdline_args.task, cmdline_args.year, cmdline_args.src_fn, dwh_manager
    )

    # exec tasks
    if not cmdline_args.dryrun:
        dispatch(pm_builder)


# -----------------------------------------------------------------------------
if __name__ == "__main__":

    # logging
    FORMAT = (
        "%(asctime)-15s %(levelname)-8s | %(lineno)d %(filename)s: %(message)s"
    )
    logging.basicConfig(level=logging.DEBUG, format=FORMAT)

    main()
