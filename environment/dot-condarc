# Conda rc file for the impulse environment
# Copy this file to ~/.condarc in the developer's local env.
# See https://conda.io/projects/conda/en/latest/user-guide/configuration/use-condarc.html
# This file format follows a yaml structure.

# Specify directories in which environments are located.
envs_dirs:
  - ~/.conda/envs


# Specify directories in which packages are located. If this key 
# is set, the root prefix pkgs_dirs is not used unless explicitly
# included.
# pkgs_dirs

# channel_priority (ChannelPriority)
#   Accepts values of 'strict', 'flexible', and 'disabled'. The default
#   value is 'flexible'. With strict channel priority, packages in lower
#   priority channels are not considered if a package with the same name
#   appears in a higher priority channel. With flexible channel priority,
#   the solver may reach into lower priority channels to fulfill
#   dependencies, rather than raising an unsatisfiable error. With channel
#   priority disabled, package version takes precedence, and the
#   configured priority of channels is used only to break ties. In
#   previous versions of conda, this parameter was configured as either
#   True or False. True is now an alias to 'flexible'.
# 
# impulse-node: generated from $ conda config --describe channel_priority
channel_priority: strict

# Listing channel locations in the .condarc file overrides conda
# defaults, causing conda to search only the channels listed here, 
# in the order given. 
# Use defaults to automatically include all default channels. 
# Non-URL channels are interpreted as Anaconda.org user names. You 
# can change this by modifying the channel_alias as described in 
# Set a channel alias (channel_alias). The default is just defaults.
# impulse-note: see https://conda-forge.org/docs/user/tipsandtricks.html
channels:
  - defaults
  - conda-forge
allow_other_channels: true


# Normally the defaults channel points to several channels at the
# repo.continuum.io repository, but if default_channels is defined, 
# it sets the new list of default channels. This is especially 
# useful for air gap and enterprise installations:
# default_channels: 


# When True, conda updates itself any time a user updates or 
# installs a package in the root environment. 
auto_update_conda: true


# Choose the yes option whenever asked to proceed, such as 
# when installing. 
always_yes: false


# Show channel URLs when displaying what is going to be downloaded 
# and in conda list
show_channel_urls: true


# When using conda activate, change the command prompt from $PS1 
# to include the activated environment. 
changeps1: false


# Add pip, wheel and setuptools as dependencies of Python. This 
# ensures that pip, wheel and setuptools are always installed any 
# time Python is installed. The default is True.
add_pip_as_python_dependency: true


# Use pip when listing packages with conda list. This does not affect
# any conda command or functionality other than the output of the
# command conda list. The default is True.
use_pip: true


# By default, proxy settings are pulled from the HTTP_PROXY 
# and HTTPS_PROXY environment variables or the system. Setting
# them here overrides that default:
# proxy_servers:


# If you are behind a proxy that does SSL inspection such as a 
# Cisco IronPort Web Security Appliance (WSA), you may need to use
# ssl_verify to override the SSL verification settings.
ssl_verify: true


# Filters out all channel URLs that do not use the file:// protocol. 
# The default is False.
offline: false


# When allow_softlinks is True, conda uses hard-links when possible 
# and soft-links---symlinks---when hard-links are not possible, 
# such as when installing on a different file system than the one 
# that the package cache is on.
# When allow_softlinks is False, conda still uses hard-links when 
# possible, but when it is not possible, conda copies files. 
# Individual packages can override this option, specifying that 
# certain files should never be soft-linked.
# The default is True.
allow_softlinks: true


# When creating new environments, add the specified packages by
# default. The default packages are installed in every environment
# you create. The default is to not include any packages.
create_default_packages:
  - pip
  - numpy
  - scipy
  - pandas
  - matplotlib
  - seaborn
  - jupyter
  - nb_conda
  - black
  - flake8-black
  - bump2version
  - twine
  - coverage
  - flake8
  - tox-conda
  - parameterized


# Enable certain features to be tracked by default. The default
# is to not track any features. This is similar to adding mkl
# to the create_default_packages list.
track_features:
  - mkl


# By default, conda install updates the given package to the latest
# version and installs any dependencies necessary for that package.
# However, if dependencies that satisfy the package's requirements 
# are already installed, conda will not update those packages to
# the latest version.
# In this case, if you would prefer that conda update all dependencies 
# to the latest version that is compatible with the environment, set
# update_dependencies to True.
update_dependencies: true


# Disallow the installation of certain packages. The default is to
# allow installation of all packages.
# disallow:




