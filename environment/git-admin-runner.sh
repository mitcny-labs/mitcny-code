#!/bin/bash

# Installs executables related to the git env for impulse-scape.
# This executable should be run on the client side after a git clone.
# There is no post-clone git hook since that is a security 
# vulnerability, so this exec effectively acts as a post-clone
# exec, only it must be called by the user directly in his or her repo.

# Example:
#   $ git-admin -t install-hooks

# usage message
function usage \
{
    echo "usage: $0 -t TASK [-h]"
    echo "  TASK:  {install-hooks} "
    echo "  -h:    help message    "
    exit
}

# parse the cmdline options
while getopts "t:h" optname
  do
    case $optname in
      t)
        TASK=$OPTARG ;;
      h)
        usage ;;
      \?)
        echo "Unknown task $OPTARG"
        usage ;;
      :)
        echo "No argument value for option $OPTARG"
        usage ;;
      *)
        echo "Unknown error while processing options"
        usage ;;
    esac
done

# switch on task
case $TASK in

    install-hooks)
        
        GIT_HOOKS_REL_PATH="../.git/hooks"

        # validate that the hooks rel path exists
        if [ ! -d $GIT_HOOKS_REL_PATH ]; then
            echo "$GIT_HOOKS_REL_PATH path not found. exiting."
            exit 1
        fi

        # copy each file in hooks/ to .git/hooks, apply chmod
        for f in `find git-hooks -type f`; do
            fn=`basename $f`
            cp $f $GIT_HOOKS_REL_PATH
            chmod +x $GIT_HOOKS_REL_PATH/$fn
            echo "installed $GIT_HOOKS_REL_PATH/$fn"
        done

        ;;

    *)
        echo "unknown task"
        usage ;;

esac




